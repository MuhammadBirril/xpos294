﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace ViewModel
{
    public class AccountViewModel
    {
        public long Id { get; set; }
        [Key, Required, MaxLength(50)]
        public string Username { get; set; }
        [Required, MaxLength(200)]
        public string Password { get; set; }
        [Required, MaxLength(50)]
        public string FirstName { get; set; }
        [Required, MaxLength(50)]
        public string Lastname { get; set; }
        public bool Active { get; set; }
    }
}
