﻿//using System;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System.Threading.Tasks;
//using WebApi.Models;
//using System.Collections.Generic;
//using System.Linq;
//using ViewModel;
//using WebApi.Repositories;

//namespace WebApi.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class AccountController : ControllerBase
//    {
//        private AccountRepository accountRepo = new AccountRepository();

//        [HttpGet]
//        public async Task<List<AccountViewModel>> Get()
//        {
//            return accountRepo.GetAll();
//        }

//        [HttpGet("{id}")]
//        public async Task<AccountViewModel> Get(long id)
//        {
//            return accountRepo.GetById(id);
//        }

//        [HttpPost]
//        public async Task<AccountViewModel> Post(AccountViewModel model)
//        {
//            var result = accountRepo.Create(model);
//            if (!result)
//                return new AccountViewModel();
//            return model;
//        }

//        [HttpPut]
//        public async Task<AccountViewModel> Put(AccountViewModel model)
//        {
//            var result = accountRepo.Update(model);
//            if (!result)
//                return new AccountViewModel();
//            return model;
//        }

//        [HttpDelete]
//        public async Task<AccountViewModel> Delete(AccountViewModel model)
//        {
//            var result = accountRepo.Delete(model);
//            if (!result)
//                return new AccountViewModel();
//            return model;
//        }
//    }
//}
