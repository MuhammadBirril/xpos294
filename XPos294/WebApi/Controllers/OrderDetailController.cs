﻿//using System;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using System.Threading.Tasks;
//using WebApi.Models;
//using System.Collections.Generic;
//using System.Linq;
//using ViewModel;
//using WebApi.Repositories;

//namespace WebApi.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class OrderDetailController : ControllerBase
//    {
//        private OrderDetailRepository orderDetailRepo = new OrderDetailRepository();

//        [HttpGet]
//        public async Task<List<OrderDetailViewModel>> Get()
//        {
//            return orderDetailRepo.GetAll();
//        }

//        [HttpGet("{id}")]
//        public async Task<OrderDetailViewModel> Get(long id)
//        {
//            return orderDetailRepo.GetById(id);
//        }

//        [HttpPost]
//        public async Task<OrderDetailViewModel> Post(OrderDetailViewModel model)
//        {
//            var result = orderDetailRepo.Create(model);
//            if (!result)
//                return new OrderDetailViewModel();
//            return model;
//        }

//        [HttpPut]
//        public async Task<OrderDetailViewModel> Put(OrderDetailViewModel model)
//        {
//            var result = orderDetailRepo.Update(model);
//            if (!result)
//                return new OrderDetailViewModel();
//            return model;
//        }

//        [HttpDelete]
//        public async Task<OrderDetailViewModel> Delete(OrderDetailViewModel model)
//        {
//            var result = orderDetailRepo.Delete(model);
//            if (!result)
//                return new OrderDetailViewModel();
//            return model;
//        }
//    }
//}