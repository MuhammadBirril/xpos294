﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HewanController : ControllerBase
    {
        [HttpGet]
        public async Task<Hewan> Get()
        {
            Hewan hewan = new Hewan();
            hewan.Nama = "Kuda";
            hewan.Warna = "Putih";
            hewan.jumlahKaki = 4;
            hewan.hidup = "Darat";
            hewan.aktifMalam = false;
            hewan.pemakan = "Herbivora";
            hewan.terbang = false;


            return hewan;
        }
    }
}
