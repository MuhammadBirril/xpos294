﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuahController : ControllerBase
    {
        [HttpGet]
        public async Task<Buah> Get()
        {
            Buah buah = new Buah();
            buah.Nama = "Pisang";
            buah.Warna = "Kuning";
            buah.Vitamin = "C";
            buah.Kalori = 89;
            buah.berair = true;
            
            return buah;
        }
    }
}
