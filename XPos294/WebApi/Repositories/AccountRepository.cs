﻿using System;
using System.Collections.Generic;
using ViewModel;
using System.Linq;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class AccountRepository : IRepositories<AccountViewModel>
    {
        private XposDbContext _XPosDbContext = new XposDbContext();
        private ResponseResult result = new ResponseResult();

        private string _UserName;

        public AccountRepository()
        {
            _UserName = "Birril";
        }

        public AccountRepository(string username)
        {
            _UserName = username;
        }

        public ResponseResult Create(AccountViewModel entity)
        {
            try
            {
                Account item = new Account();
                item.Username = entity.Username;
                item.Password = entity.Password;
                item.FirstName = entity.FirstName;
                item.Lastname = entity.Lastname;
                item.Active = entity.Active;

                item.CreateBy = _UserName;
                item.CreateDate = DateTime.Now;

                _XPosDbContext.Accounts.Add(item);
                _XPosDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(AccountViewModel entity)
        {
            try
            {
                Account item = _XPosDbContext.Accounts
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                if (item != null)
                {
                    result.Entity = item;
                    _XPosDbContext.Accounts.Remove(item);
                    _XPosDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<AccountViewModel> GetAll()
        {
            List<AccountViewModel> result = new List<AccountViewModel>();
            try
            {
                result = (from o in _XPosDbContext.Accounts
                          select new AccountViewModel
                          {
                              Id = o.Id,
                              Username = o.Username,
                              Password = o.Password,
                              FirstName = o.FirstName,
                              Lastname = o.Lastname,
                              Active = o.Active
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public AccountViewModel GetById(long id)
        {
            AccountViewModel result = new AccountViewModel();
            try
            {
                result = (from o in _XPosDbContext.Accounts
                          where o.Id == id
                          select new AccountViewModel
                          {
                              Id = o.Id,
                              Username = o.Username,
                              Password = o.Password,
                              FirstName = o.FirstName,
                              Lastname = o.Lastname,
                              Active = o.Active
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(AccountViewModel entity)
        {
            try
            {
                Account item = _XPosDbContext.Accounts
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.Username = entity.Username;
                    item.Password = entity.Password;
                    item.FirstName = entity.FirstName;
                    item.Lastname = entity.Lastname;
                    item.Active = entity.Active;

                    item.ModifyBy = _UserName;
                    item.ModifyDate = DateTime.Now;

                    _XPosDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }
}
