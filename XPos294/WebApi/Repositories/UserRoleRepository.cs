﻿using System;
using System.Collections.Generic;
using ViewModel;
using System.Linq;
using WebApi.DataModels;

namespace WebApi.Repositories
{
    public class UserRoleRepository : IRepositories<UserRoleViewModel>
    {
        private XposDbContext _XPosDbContext = new XposDbContext();
        private ResponseResult result = new ResponseResult();

        private string _UserName;

        public UserRoleRepository()
        {
            _UserName = "Birril";
        }

        public UserRoleRepository(string username)
        {
            _UserName = username;
        }

        public ResponseResult Create(UserRoleViewModel entity)
        {
            try
            {
                UserRole item = new UserRole();
                item.Username = entity.Username;
                item.Role = entity.Role;
                item.Active = entity.Active;

                item.CreateBy = _UserName;
                item.CreateDate = DateTime.Now;

                _XPosDbContext.UserRoles.Add(item);
                _XPosDbContext.SaveChanges();

                result.Entity = item;
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public ResponseResult Delete(UserRoleViewModel entity)
        {
            try
            {
                UserRole item = _XPosDbContext.UserRoles
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                if (item != null)
                {
                    result.Entity = item;
                    _XPosDbContext.UserRoles.Remove(item);
                    _XPosDbContext.SaveChanges();
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public List<UserRoleViewModel> GetAll()
        {
            List<UserRoleViewModel> result = new List<UserRoleViewModel>();
            try
            {
                result = (from o in _XPosDbContext.UserRoles
                          select new UserRoleViewModel
                          {
                              Id = o.Id,
                              Username = o.Username,
                              Role = o.Role,
                              Active = o.Active
                          }).ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public UserRoleViewModel GetById(long id)
        {
            UserRoleViewModel result = new UserRoleViewModel();
            try
            {
                result = (from o in _XPosDbContext.UserRoles
                          where o.Id == id
                          select new UserRoleViewModel
                          {
                              Id = o.Id,
                              Username = o.Username,
                              Role = o.Role,
                              Active = o.Active
                          }).FirstOrDefault();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
            return result;
        }

        public ResponseResult Update(UserRoleViewModel entity)
        {
            try
            {
                UserRole item = _XPosDbContext.UserRoles
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                if (item != null)
                {
                    item.Username = entity.Username;
                    item.Role = entity.Role;
                    item.Active = entity.Active;

                    item.ModifyBy = _UserName;
                    item.ModifyDate = DateTime.Now;

                    _XPosDbContext.SaveChanges();
                    result.Entity = item;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Not found";
                    result.Entity = entity;
                }
            }
            catch (Exception e)
            {
                result.Success = false;
                result.Message = e.Message;
            }
            return result;
        }
    }
}
