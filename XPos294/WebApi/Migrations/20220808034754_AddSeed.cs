﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class AddSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Categories_VariantId",
                table: "Products");

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Username = table.Column<string>(maxLength: 50, nullable: false),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: true),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Password = table.Column<string>(maxLength: 200, nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    Lastname = table.Column<string>(maxLength: 50, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Username);
                });

            migrationBuilder.CreateTable(
                name: "FileCollections",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: false),
                    FileName = table.Column<string>(maxLength: 200, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileCollections", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: true),
                    HeaderId = table.Column<long>(nullable: true),
                    ProductId = table.Column<long>(nullable: false),
                    Quantity = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_OrderHeaders_HeaderId",
                        column: x => x.HeaderId,
                        principalTable: "OrderHeaders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderDetails_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateBy = table.Column<string>(maxLength: 50, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModifyBy = table.Column<string>(maxLength: 50, nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: true),
                    Username = table.Column<string>(maxLength: 50, nullable: false),
                    Role = table.Column<string>(maxLength: 100, nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoles_Accounts_Username",
                        column: x => x.Username,
                        principalTable: "Accounts",
                        principalColumn: "Username",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Active", "CreateBy", "CreateDate", "Initial", "ModifyBy", "ModifyDate", "Name" },
                values: new object[] { 1L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 980, DateTimeKind.Local).AddTicks(2499), "MainCo", null, null, "Main Course" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Active", "CreateBy", "CreateDate", "Initial", "ModifyBy", "ModifyDate", "Name" },
                values: new object[] { 2L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 981, DateTimeKind.Local).AddTicks(3362), "Drink", null, null, "Drink" });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Active", "CreateBy", "CreateDate", "Initial", "ModifyBy", "ModifyDate", "Name" },
                values: new object[] { 3L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 981, DateTimeKind.Local).AddTicks(3407), "Dessert", null, null, "Dessert" });

            migrationBuilder.InsertData(
                table: "Variants",
                columns: new[] { "Id", "Active", "CategoryId", "CreateBy", "CreateDate", "Initial", "ModifyBy", "ModifyDate", "Name" },
                values: new object[,]
                {
                    { 1L, true, 1L, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 982, DateTimeKind.Local).AddTicks(8906), "Paket Nasi", null, null, "Paket Nasi" },
                    { 2L, true, 1L, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 982, DateTimeKind.Local).AddTicks(9048), "Ala Carte", null, null, "Ala Carte" },
                    { 3L, true, 1L, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 982, DateTimeKind.Local).AddTicks(9052), "Favorite", null, null, "Favorite" },
                    { 4L, true, 2L, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 982, DateTimeKind.Local).AddTicks(9055), "Iced", null, null, "Iced" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Active", "CreateBy", "CreateDate", "Description", "Initial", "ModifyBy", "ModifyDate", "Name", "Price", "Stock", "VariantId" },
                values: new object[,]
                {
                    { 1L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 983, DateTimeKind.Local).AddTicks(1829), "Capcay seafood", "NasCap", null, null, "Nasi Capcay", 25000m, 10m, 1L },
                    { 4L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 983, DateTimeKind.Local).AddTicks(2001), "1/2 ekor", "AyGor", null, null, "Ayam Goreng", 18000m, 10m, 2L },
                    { 2L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 983, DateTimeKind.Local).AddTicks(1963), "Dengan bumbu rempah", "AyKal", null, null, "Ayam Kalasan", 26000m, 10m, 3L },
                    { 3L, true, "Admin", new DateTime(2022, 8, 8, 10, 47, 53, 983, DateTimeKind.Local).AddTicks(1968), "Dengan gula aren", "KaMer", null, null, "Iced Kacang Merah", 18000m, 10m, 4L }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_Username",
                table: "Accounts",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FileCollections_FileName",
                table: "FileCollections",
                column: "FileName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FileCollections_Title",
                table: "FileCollections",
                column: "Title",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_HeaderId",
                table: "OrderDetails",
                column: "HeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_ProductId",
                table: "OrderDetails",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_Username",
                table: "UserRoles",
                column: "Username");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Variants_VariantId",
                table: "Products",
                column: "VariantId",
                principalTable: "Variants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Variants_VariantId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "FileCollections");

            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Variants",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Categories_VariantId",
                table: "Products",
                column: "VariantId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
